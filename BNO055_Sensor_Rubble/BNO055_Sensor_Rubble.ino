#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32.h>


////////////////
// ROS Topics //
////////////////

ros::NodeHandle nh;

// Topics to publish
std_msgs::Float32 axis_x;
std_msgs::Float32 axis_y;
std_msgs::Float32 axis_z;
std_msgs::Bool stat;

// Publishing objects
ros::Publisher axis_x_pub("rubble/dof/axis/x", &axis_x);
ros::Publisher axis_y_pub("rubble/dof/axis/y", &axis_y);
ros::Publisher axis_z_pub("rubble/dof/axis/z", &axis_z);
ros::Publisher stat_pub("rubble/dof/status", &stat);

Adafruit_BNO055 bno = Adafruit_BNO055(55);
 
void setup(void) 
{

  // Initialize the ROS node
  nh.initNode();
  
  // Be sure you can publish
  nh.advertise(axis_x_pub);
  nh.advertise(axis_y_pub);
  nh.advertise(axis_z_pub);
  nh.advertise(stat_pub);

  // 'true' means the sensor is working
  //stat_pub.publish(true);
  
  /* Initialise the sensor */
  if(!bno.begin())
  {
    // If we cannot init the sensor, publish 'false'
  //  stat_pub.publish(false)
  }
  
  delay(1000);
  bno.setExtCrystalUse(true);
}
 
void loop(void) 
{
  
  
  /* Get a new sensor event */ 
  sensors_event_t event; 
  bno.getEvent(&event);

  axis_x.data = event.orientation.x;
  axis_y.data = event.orientation.y;
  axis_z.data = event.orientation.z;
  

  /* Publish to ROS topics */
  axis_x_pub.publish( &axis_x );
  axis_y_pub.publish( &axis_y );
  axis_z_pub.publish( &axis_z );

  nh.spinOnce();
  
  delay(200);
}
